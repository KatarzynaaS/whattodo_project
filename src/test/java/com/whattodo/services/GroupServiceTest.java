package com.whattodo.services;

import com.whattodo.dto.CreateGroupDto;
import com.whattodo.dto.GroupMapper;
import com.whattodo.exception.AlreadyExists;
import com.whattodo.exception.InvalidData;
import com.whattodo.exception.NotFound;
import com.whattodo.model.UserGroup;
import com.whattodo.model.Task;
import com.whattodo.model.User;
import com.whattodo.repository.UserGroupRepository;
import com.whattodo.repository.TaskRepository;
import com.whattodo.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class GroupServiceTest {

    @Mock
    private UserGroupRepository userGroupRepository;
    @Mock
    private TaskRepository taskRepository;
    @Mock
    private UserRepository userRepository;
    @InjectMocks
    private GroupService groupService;
    @Spy
    private GroupMapper groupMapper = new GroupMapper();

    @Test
    void createNewGroupShouldSavedGroup() throws InvalidData, AlreadyExists, NotFound {

        when(userGroupRepository.existsUserGroupByGroupName("abc")).thenReturn(false);
        CreateGroupDto createGroupDto = new CreateGroupDto("abc", "abc");
        groupService.createNewGroup(createGroupDto);
        verify(userGroupRepository, times(1)).save(any(UserGroup.class));
    }

    @Test
    void createNewGroupShouldThrowExceptionGroupNameAlreadyExist() throws InvalidData, AlreadyExists, NotFound {

        CreateGroupDto createGroupDto = new CreateGroupDto("abc", "abc");
        when(userGroupRepository.existsUserGroupByGroupName("abc")).thenReturn(true);
        assertThrows(AlreadyExists.class, () -> {
            groupService.createNewGroup(createGroupDto);
        });
        verify(userGroupRepository, times(0)).save(any(UserGroup.class));
    }

    @Test
    void shouldDeleteMyGroupByName() throws NotFound {

        UserGroup group = new UserGroup(1, "abc", "abc", new ArrayList<>(), new ArrayList<>());
        when(userGroupRepository.findByGroupName("abc")).thenReturn(java.util.Optional.of(group));
        groupService.deleteGroup("abc");
        verify(userRepository, times(0)).delete(any(User.class));
        verify(taskRepository, times(0)).delete(any(Task.class));
        verify(userGroupRepository, times(1)).delete(any(UserGroup.class));
    }
}
