package com.whattodo.services;

import com.whattodo.dto.CreateTaskDto;
import com.whattodo.dto.TaskDto;
import com.whattodo.dto.TaskMapper;
import com.whattodo.dto.UpdatedTaskDto;
import com.whattodo.exception.AlreadyExists;
import com.whattodo.exception.NotFound;
import com.whattodo.model.*;
import com.whattodo.repository.UserGroupRepository;
import com.whattodo.repository.TaskRepository;
import com.whattodo.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class TaskServiceTest {

    @Mock
    private UserRepository userRepository;
    @Mock
    private TaskRepository taskRepository;
    @Mock
    private UserGroupRepository userGroupRepository;
    @Spy
    private TaskMapper taskMapper = new TaskMapper();
    @InjectMocks
    private TaskService taskService;
    @Captor
    private ArgumentCaptor<Task> taskCaptor;

    @Test
    void getAllUsersByGroupId_shouldReturnCorrectDtoList_empty() {

        when(taskRepository.findAll()).thenReturn(new ArrayList<>());
        List<TaskDto> allTasks = taskService.getAllTasks(1);
        assertThat(allTasks.size()).isEqualTo(0);
    }

    @Test
    void createTask_shouldCreate() throws NotFound, AlreadyExists {

        UserGroup userGroup = new UserGroup(1, "abc", "abc", new ArrayList<>(), new ArrayList<>());
        when(userGroupRepository.findById(1)).thenReturn(java.util.Optional.of(userGroup));
        User user = new User(1, "jan", "passwordJan", Role.ADMIN, 0L,
                new ArrayList<>(), new ArrayList<>(), userGroup);
        CreateTaskDto createTaskDto = new CreateTaskDto("task1", "task1description", "EASE", "jan");
        when(userRepository.findByLogin("jan")).thenReturn(java.util.Optional.of(user));
        taskService.createNewTask(createTaskDto, "jan");
        verify(taskRepository, times(1)).save(any(Task.class));
    }

    @Test
    void createTask_shouldCreateTaskWithStatusInactived() throws NotFound, AlreadyExists {

        UserGroup userGroup = new UserGroup(1, "abc", "abc", new ArrayList<>(), new ArrayList<>());
        when(userGroupRepository.findById(1)).thenReturn(java.util.Optional.of(userGroup));
        User user = new User(1, "jan", "passwordJan", Role.ADMIN, 0L,
                new ArrayList<>(), new ArrayList<>(), userGroup);
        when(userRepository.findByLogin("jan")).thenReturn(java.util.Optional.of(user));
        userGroup.getUsers().add(user);
        CreateTaskDto createTaskDto = new CreateTaskDto("task1", "task1description", "EASE", "jan");
        when(taskRepository.save(taskCaptor.capture())).thenReturn(null);
        taskService.createNewTask(createTaskDto, "jan");
        Task savedTask = taskCaptor.getValue();
        assertThat(savedTask.getStatus()).isEqualTo(TaskStatus.INACTIVATED);
    }


    @Test
    void activateTask_shouldSetActivatedTaskStatus() throws NotFound {

        Task task = new Task(1, "task1", "task1Description", Level.EASE, new User(), null,
                TaskStatus.INACTIVATED, new UserGroup());
        when(taskRepository.findById(1)).thenReturn(java.util.Optional.of(task));
        when(taskRepository.save(taskCaptor.capture())).thenReturn(task);
        taskService.activateTask(task.getId());
        assertThat(task.getStatus()).isEqualTo(TaskStatus.ACTIVATED);
    }

    @Test
    void startedTask_shouldSetStartedTaskStatus() throws NotFound {

        UserGroup userGroup = new UserGroup();
        Task task = new Task(1, "task1", "task1Description", Level.EASE, new User(), null,
                TaskStatus.ACTIVATED, userGroup);
        User user = new User(1, "jan", "passwordJan", Role.ADMIN, 0L, new ArrayList<>(),
                new ArrayList<>(), userGroup);
        when(taskRepository.findById(1)).thenReturn(java.util.Optional.of(task));
        when(userRepository.findByLogin("jan")).thenReturn(java.util.Optional.of(user));
        when(taskRepository.save(taskCaptor.capture())).thenReturn(task);
        taskService.startTask(task.getId(), user.getLogin());
        assertThat(task.getStatus()).isEqualTo(TaskStatus.STARTED);
    }

    @Test
    void finishedTask_shouldSetFinishedTaskStatus() throws NotFound {

        UserGroup userGroup = new UserGroup();
        Task task = new Task(1, "task1", "task1Description",
                Level.EASE, new User(), new User(), TaskStatus.STARTED, userGroup);
        when(taskRepository.findById(1)).thenReturn(java.util.Optional.of(task));
        taskService.finishTask(task.getId());
        assertThat(task.getStatus()).isEqualTo(TaskStatus.FINISHED);
    }

    @Test
    void finishedAcceptedTask_shouldSetFinishedAcceptedTaskStatus() throws NotFound {

        UserGroup userGroup = new UserGroup();
        User user = new User(1, "jan", "passwordJan", Role.ADMIN, 0L, new ArrayList<>(), new ArrayList<>(), userGroup);
        Task task = new Task(1, "task1", "task1Description", Level.EASE, new User(), user, TaskStatus.FINISHED, userGroup);
        when(taskRepository.findById(1)).thenReturn(java.util.Optional.of(task));
        taskService.finishedAcceptedTask(task.getId());
        assertThat(task.getStatus()).isEqualTo(TaskStatus.FINISHED_ACCEPTED);
        assertThat(user.getPoints()).isEqualTo(1);
    }

    @Test
    void updateTask_shouldUpdatedTask() throws NotFound {

        UserGroup userGroup = new UserGroup();
        Task task = new Task(1, "task1", "task1Description", Level.EASE, new User(), null,
                TaskStatus.INACTIVATED, userGroup);
        when(taskRepository.findById(1)).thenReturn(java.util.Optional.of(task));
        UpdatedTaskDto updatedTaskDto = new UpdatedTaskDto("newName", "newDescription", Level.MEDIUM);
        taskService.updateTask(task.getId(), updatedTaskDto);
        assertThat(task.getName()).isEqualTo(updatedTaskDto.getName());
        assertThat(task.getDescription()).isEqualTo(updatedTaskDto.getDescription());
        assertThat(task.getLevel()).isEqualTo(updatedTaskDto.getLevel());
    }
}