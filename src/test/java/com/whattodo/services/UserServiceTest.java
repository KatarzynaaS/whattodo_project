package com.whattodo.services;

import com.whattodo.dto.CreateUserDto;
import com.whattodo.dto.UserDto;
import com.whattodo.dto.UserMapper;
import com.whattodo.exception.AlreadyExists;
import com.whattodo.exception.InvalidData;
import com.whattodo.exception.NotFound;
import com.whattodo.model.UserGroup;
import com.whattodo.model.Role;
import com.whattodo.model.Task;
import com.whattodo.model.User;
import com.whattodo.repository.UserGroupRepository;
import com.whattodo.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class UserServiceTest {

    @Mock
    private UserRepository userRepository;
    @Mock
    private UserGroupRepository userGroupRepository;

    @Spy
    private UserMapper userMapper = new UserMapper();

    @InjectMocks
    private UserService userService;

    @Captor
    private ArgumentCaptor<String> argumentCaptor;

    @Test
    void getAllUsersByGroupId_shouldReturnCorrectDtoList_empty() throws NotFound {

        when(userRepository.findAll()).thenReturn(new ArrayList<>());
        List<UserDto> allUsers = userService.getAllUsersByGroupId(1);
        assertThat(allUsers.size()).isEqualTo(0);
    }


    @Test
    void createNewUser_shouldThrowExceptionWhenLoginExists() throws InvalidData, AlreadyExists {

        CreateUserDto createUserDto = CreateUserDto.builder().login("janek").build();
        when(userRepository.existsByLogin("janek")).thenReturn(true);
        assertThrows(AlreadyExists.class, () -> {
            userService.createNewUser(createUserDto);
        });
        verify(userRepository, times(0)).save(any(User.class));
    }

    @Test
    void addNewUser_shouldSaveUser() throws InvalidData, AlreadyExists, NotFound {

        UserGroup userGroup = new UserGroup(1, "abc", "abc", new ArrayList<>(), new ArrayList<>());
        when(userGroupRepository.findByGroupName("abc")).thenReturn(java.util.Optional.of(userGroup));
        CreateUserDto createUserDto = new CreateUserDto("ala", "password", "password",
                "ADMIN", "abc", "abc");
        when(userRepository.existsByLogin("ala")).thenReturn(false);
        userService.createNewUser(createUserDto);
        verify(userRepository, times(1)).save(any(User.class));
    }

    @Test
    void addNewUser_shouldThrowExceptionWhenPasswordAndRePasswordAreNoTheSame() throws InvalidData, AlreadyExists {

        CreateUserDto createUserDto = new CreateUserDto("janek", "password", "password1",
                "ADMIN", "abc", "abc");
        when(userRepository.existsByLogin("janek")).thenReturn(false);
        assertThrows(InvalidData.class, () -> {
            userService.createNewUser(createUserDto);
        });
        verify(userRepository, times(0)).save(any(User.class));
    }

    @Test
    void shouldDeleteUserByLogin() throws NotFound {

        UserGroup userGroup = new UserGroup(1, "abc", "abc", new ArrayList<User>(), new ArrayList<Task>());
        User user = new User(1, "Anna", "passwordAnna", Role.ADMIN, 0L, new ArrayList<>(), new ArrayList<>(), userGroup);
        when(userRepository.findByLogin("Anna")).thenReturn(java.util.Optional.of(user));
        userService.deleteUser("Anna");
        verify(userRepository, times(1)).delete(any(User.class));
    }

    @Test
    void testArgumentCaptor() throws NotFound {

        UserGroup userGroup = new UserGroup(1, "abc", "abc", new ArrayList<User>(), new ArrayList<Task>());
        User user = new User(1, "Janek", "passwordAnna", Role.ADMIN,
                0L, new ArrayList<>(), new ArrayList<>(), userGroup);
        user.setLogin("Anna");
        when(userRepository.findByLogin(argumentCaptor.capture())).thenReturn(java.util.Optional.of(user));
        userService.getByLogin("Anna");
        assertThat(argumentCaptor.getValue()).isEqualTo("Anna");
    }
}