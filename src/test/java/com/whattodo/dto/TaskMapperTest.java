package com.whattodo.dto;

import com.whattodo.model.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
class TaskMapperTest {

    @InjectMocks
    TaskMapper taskMapper = new TaskMapper();

    @Test
    void shouldCreateTaskDtoFromTask() {

        User user = new User();
        UserGroup userGroup = new UserGroup();
        Task task = new Task(1, "task1", "description1", Level.MEDIUM, user, null,
                TaskStatus.INACTIVATED, userGroup);
        TaskDto taskDto = taskMapper.taskToDto(task);
        assertEquals(new TaskDto(1, "task1", "description1", Level.MEDIUM, user.getLogin(), "---", TaskStatus.INACTIVATED, userGroup.getId()),
                taskDto);
    }
}