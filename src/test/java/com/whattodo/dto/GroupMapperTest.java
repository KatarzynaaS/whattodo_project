package com.whattodo.dto;

import com.whattodo.model.UserGroup;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
class GroupMapperTest {

    @InjectMocks
    GroupMapper groupMapper;

    @Test
    void shouldCreateMyGroupDtoFromMyGroup() {

        UserGroup userGroup = new UserGroup(1, "abc", "abc", new ArrayList<>(), new ArrayList<>());
        GroupDto groupDto = groupMapper.toDtoMyGroup(userGroup);
        assertEquals(new GroupDto(1, "abc", "abc", new ArrayList<>(), new ArrayList<>()), groupDto);
    }
}

