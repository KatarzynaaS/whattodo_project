package com.whattodo.dto;

import com.whattodo.model.UserGroup;
import com.whattodo.model.Role;
import com.whattodo.model.User;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
class UserMapperTest {

    @InjectMocks
    private UserMapper userMapper = new UserMapper();

    @Test
    void shouldCreateUserDtoFromUser() {

        UserGroup userGroup = new UserGroup();
        User user = new User(1, "Anna", "passwordAnna", Role.ADMIN, 0L, new ArrayList<>(), new ArrayList<>(), userGroup);
        UserDto userDto = userMapper.toDtoUser(user);
        assertEquals(new UserDto(1, "Anna", "passwordAnna", Role.ADMIN, 0L, new ArrayList<>(), new ArrayList<>(), userGroup.getId()), userDto);
    }
}
