package com.whattodo.repository;

import com.whattodo.model.UserGroup;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserGroupRepository extends JpaRepository<UserGroup, Integer> {

    boolean existsUserGroupByGroupName(String groupName);

    Optional<UserGroup> findByGroupName(String groupName);
}