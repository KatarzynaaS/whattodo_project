package com.whattodo.model;

public enum TaskStatus {

    INACTIVATED, ACTIVATED, STARTED, FINISHED, FINISHED_ACCEPTED
}