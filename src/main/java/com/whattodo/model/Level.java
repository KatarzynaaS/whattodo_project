package com.whattodo.model;

public enum Level {

    EASE, MEDIUM, HARD
}