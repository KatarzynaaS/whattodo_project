package com.whattodo.model;

public enum Role {

    ADMIN, JUNIOR, MIDDLE, SENIOR
}
