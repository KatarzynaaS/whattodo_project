package com.whattodo.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
public class Task {

    @GeneratedValue(strategy = GenerationType.AUTO)
    @Id
    private Integer id;
    private String name;
    private String description;
    @Enumerated(EnumType.STRING)
    private Level level;
    @ManyToOne
    private User whoAdded;
    @ManyToOne
    private User whoDo;
    private TaskStatus status;
    @ManyToOne
    private UserGroup taskGroup;
}