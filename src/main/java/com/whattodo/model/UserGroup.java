package com.whattodo.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
public class UserGroup {

    @GeneratedValue(strategy = GenerationType.AUTO)
    @Id
    private Integer id;
    private String groupName;
    private String groupKey;
    @Builder.Default
    @OneToMany(mappedBy = "userGroup")
    private List<User> users = new ArrayList<>();
    @Builder.Default
    @OneToMany(mappedBy = "taskGroup")
    private List<Task> tasks = new ArrayList<>();
}