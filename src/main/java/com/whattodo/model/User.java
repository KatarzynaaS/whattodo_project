package com.whattodo.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
public class User {

    @GeneratedValue(strategy = GenerationType.AUTO)
    @Id
    private Integer id;
    private String login;
    private String password;
    @Enumerated(EnumType.STRING)
    private Role role;
    private Long points;
    @Builder.Default
    @OneToMany(mappedBy = "whoAdded")
    private List<Task> addedTasks = new ArrayList<>();
    @Builder.Default
    @OneToMany(mappedBy = "whoDo")
    private List<Task> tasks = new ArrayList<>();
    @ManyToOne
    private UserGroup userGroup = new UserGroup();
}