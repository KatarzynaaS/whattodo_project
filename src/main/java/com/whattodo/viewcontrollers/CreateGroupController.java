package com.whattodo.viewcontrollers;

import com.whattodo.dto.CreateGroupDto;
import com.whattodo.exception.AlreadyExists;
import com.whattodo.services.GroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;

@Controller
public class CreateGroupController {

	private final GroupService groupService;


	@Autowired
	public CreateGroupController(GroupService groupService) {

		this.groupService = groupService;
	}

	@GetMapping("/createGroup")
	public ModelAndView displayAddUserView() {

		ModelAndView mav = new ModelAndView("createGroupForm");
		CreateGroupDto createGroupDto = new CreateGroupDto();
		mav.addObject("createGroupDto", createGroupDto);

		return mav;
	}

	@PostMapping("/createGroup")
	public String createUser(@Valid @ModelAttribute CreateGroupDto createGroupDto, BindingResult bindingResult,
                             Model model) {

		if (bindingResult.hasErrors()) {
			model.addAttribute("createGroupDto", createGroupDto);

			return "createGroupForm";
		}
		try {
			groupService.createNewGroup(createGroupDto);
		}
		catch (AlreadyExists alreadyExists) {
			return "redirect:/groupNameAlreadyExistError";
		}

		String name = createGroupDto.getGroupName();

		return "redirect:/groupIsCreated" + name;
	}
}