package com.whattodo.viewcontrollers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class GroupDataErrorViewController {

    @GetMapping("/groupDataError")
    public ModelAndView groupDataErrorView() {

        ModelAndView mav = new ModelAndView("groupDataError");

        return mav;
    }
}