package com.whattodo.viewcontrollers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class LoginAlreadyExistErrorViewController {

    @GetMapping("/loginAlreadyExistError")
    public ModelAndView errorUpdateUserView() {

        ModelAndView mav = new ModelAndView("loginAlreadyExistError");

        return mav;
    }
}