package com.whattodo.viewcontrollers;

import com.whattodo.dto.GroupDto;
import com.whattodo.dto.GroupMapper;
import com.whattodo.exception.NotFound;
import com.whattodo.model.UserGroup;
import com.whattodo.repository.UserGroupRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class NewGroupWasCreatedInfoViewController {

    private final UserGroupRepository userGroupRepository;
    private final GroupMapper groupMapper;


    @Autowired
    public NewGroupWasCreatedInfoViewController(UserGroupRepository userGroupRepository,
                                                GroupMapper groupMapper) {

        this.userGroupRepository = userGroupRepository;
        this.groupMapper = groupMapper;
    }

    @GetMapping("/groupIsCreated{name}")
    public ModelAndView contentView(@PathVariable String name) throws NotFound {

        UserGroup userGroup = userGroupRepository.findByGroupName(name).orElseThrow(() -> new NotFound("not found"));
        GroupDto groupDto = groupMapper.toDtoMyGroup(userGroup);
        ModelAndView modelAndView = new ModelAndView("groupIsCreated");
        modelAndView.addObject("groupDto", groupDto);

        return modelAndView;
    }
}