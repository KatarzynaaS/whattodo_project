package com.whattodo.viewcontrollers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class DeleteCanOnlyAdminWarningViewController {

    @GetMapping("/deleteOnlyAdmin")
    public ModelAndView deleteOnlyAdminView() {

        ModelAndView mav = new ModelAndView("deleteOnlyAdmin");

        return mav;
    }
}