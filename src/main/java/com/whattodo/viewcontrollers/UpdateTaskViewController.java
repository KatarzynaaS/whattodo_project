package com.whattodo.viewcontrollers;

import com.whattodo.dto.TaskDto;
import com.whattodo.dto.TaskMapper;
import com.whattodo.dto.UpdatedTaskDto;
import com.whattodo.exception.NotFound;
import com.whattodo.model.Level;
import com.whattodo.model.Task;
import com.whattodo.repository.TaskRepository;
import com.whattodo.services.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;

@Controller
public class UpdateTaskViewController {

	private final TaskService taskService;
	private final TaskRepository taskRepository;
	private final TaskMapper taskMapper;


	@Autowired
	public UpdateTaskViewController(TaskService taskService,
	                                TaskRepository taskRepository,
	                                TaskMapper taskMapper) {

		this.taskService = taskService;
		this.taskRepository = taskRepository;
		this.taskMapper = taskMapper;
	}

	@GetMapping("/updateTask")
	public ModelAndView displayUpdateTaskView(@RequestParam Integer id) throws NotFound {

		Task task = taskRepository.findById(id).orElseThrow(() -> new NotFound("task doesn't exist"));
		TaskDto taskDto = taskMapper.taskToDto(task);
		ModelAndView mav = new ModelAndView("updateTaskForm");
		UpdatedTaskDto updatedTaskDto = new UpdatedTaskDto();
		updatedTaskDto.setName(taskDto.getName());
		updatedTaskDto.setDescription(taskDto.getDescription());
		updatedTaskDto.setLevel(taskDto.getLevel());
		mav.addObject("id", id);
		mav.addObject("updatedTaskDto", updatedTaskDto);
		mav.addObject("availableLevels", Level.values());

		return mav;
	}

	@PostMapping("/updateTask/{id}")
	public String updateTask(@PathVariable Integer id, @Valid @ModelAttribute UpdatedTaskDto updatedTaskDto,
	                         BindingResult bindingResult, Model model) {

		if (bindingResult.hasErrors()) {
			model.addAttribute("availableLevels", Level.values());
			return "updateTaskForm";
		}

		try {
			taskService.updateTask(id, updatedTaskDto);
		}
		catch (NotFound notFound) {
			notFound.printStackTrace();
		}

		return "redirect:/taskDetails?id={id}";
	}
}