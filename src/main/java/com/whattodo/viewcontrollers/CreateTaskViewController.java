package com.whattodo.viewcontrollers;

import com.whattodo.dto.CreateTaskDto;
import com.whattodo.exception.NotFound;
import com.whattodo.model.Level;
import com.whattodo.services.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;

@Controller
public class CreateTaskViewController {

	private final TaskService taskService;


	@Autowired
	public CreateTaskViewController(TaskService taskService) {

		this.taskService = taskService;
	}

	@GetMapping("/createTask")
	public ModelAndView displayAddUserView() {

		ModelAndView mav = new ModelAndView("createTaskForm");
		CreateTaskDto createTaskDto = new CreateTaskDto();
		mav.addObject("createTaskDto", createTaskDto);
		mav.addObject("availableLevels", Level.values());

		return mav;
	}

	@PostMapping("/createTask")
	public String createTask(@Valid @ModelAttribute CreateTaskDto createTaskDto, BindingResult bindingResult,
	                         Model model, Authentication authentication) {

		String loginName = authentication.getName();

		if (bindingResult.hasErrors()) {
			model.addAttribute("availableLevels", Level.values());
			model.addAttribute("createTaskDto", createTaskDto);

			return "createTaskForm";
		}

		try {
			taskService.createNewTask(createTaskDto, loginName);
		}
		catch (NotFound notFound) {
			return "redirect:/groupDataError";
		}

		return "redirect:/tasks";
	}
}