package com.whattodo.viewcontrollers;

import com.whattodo.dto.UpdatedUserDto;
import com.whattodo.exception.InvalidData;
import com.whattodo.exception.NotFound;
import com.whattodo.model.Role;
import com.whattodo.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;

@Controller
public class UpdateUserViewController {

	private final UserService userService;


	@Autowired
	public UpdateUserViewController(UserService userService) {

		this.userService = userService;
	}

	@GetMapping("/updateUser")
	public ModelAndView displayUpdateUserView() {

		ModelAndView mav = new ModelAndView("updateUserForm");
		UpdatedUserDto updatedUserDto = new UpdatedUserDto();
		mav.addObject("updateUserDto", updatedUserDto);
		mav.addObject("availableRoles", Role.values());

		return mav;
	}

	@PostMapping("/updateUser")
	public String updateUser(@Valid @ModelAttribute(name = "updateUserDto") UpdatedUserDto updatedUserDto,
	                         BindingResult bindingResult, Authentication authentication, Model model) throws NotFound {

		String login = authentication.getName();
		if (bindingResult.hasErrors()) {
			model.addAttribute("availableRoles", Role.values());

			return "updateUserForm";
		}

		try {
			userService.updateUser(updatedUserDto, login);
		}
		catch (InvalidData invalidData) {
			return "redirect:/password_re_passwordError";
		}

		return "redirect:/myProfile";
	}
}