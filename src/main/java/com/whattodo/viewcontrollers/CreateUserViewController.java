package com.whattodo.viewcontrollers;

import com.whattodo.dto.CreateUserDto;
import com.whattodo.exception.AlreadyExists;
import com.whattodo.exception.InvalidData;
import com.whattodo.exception.NotFound;
import com.whattodo.model.Role;
import com.whattodo.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;

@Controller
public class CreateUserViewController {

	private final UserService userService;


	@Autowired
	public CreateUserViewController(UserService userService) {

		this.userService = userService;
	}

	@GetMapping("/createUser")
	public ModelAndView displayAddUserView() {

		ModelAndView mav = new ModelAndView("createUserForm");
		CreateUserDto createUserDto = new CreateUserDto();
		mav.addObject("createUserDto", createUserDto);
		mav.addObject("availableRoles", Role.values());

		return mav;
	}

	@PostMapping("/createUser")
	public String createUser(@Valid @ModelAttribute CreateUserDto createUserDto,
                             BindingResult bindingResult, Model model) {

		if (bindingResult.hasErrors()) {
			model.addAttribute("availableRoles", Role.values());
			model.addAttribute("createUserDto", createUserDto);

			return "createUserForm";
		}

		try {
			userService.createNewUser(createUserDto);
		}
		catch (AlreadyExists alreadyExists) {
			return "redirect:/loginAlreadyExistError";
		}
		catch (InvalidData invalidData) {
			return "redirect:/passwordAndRepeatPasswordError";
		}
		catch (NotFound notFound) {
			return "redirect:/groupDataError";
		}

		return "redirect:/welcomeInYourGroup";
	}
}