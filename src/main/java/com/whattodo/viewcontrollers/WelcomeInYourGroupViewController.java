package com.whattodo.viewcontrollers;

import com.whattodo.dto.GroupDto;
import com.whattodo.dto.GroupMapper;
import com.whattodo.dto.UserDto;
import com.whattodo.exception.NotFound;
import com.whattodo.model.UserGroup;
import com.whattodo.repository.UserGroupRepository;
import com.whattodo.services.GroupService;
import com.whattodo.services.UserService;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
public class WelcomeInYourGroupViewController {

    private final UserService userService;
    private final GroupService groupService;
    private final UserGroupRepository userGroupRepository;
    private final GroupMapper groupMapper;


    public WelcomeInYourGroupViewController(UserService userService,
                                            GroupService groupService,
                                            UserGroupRepository userGroupRepository,
                                            GroupMapper groupMapper) {

        this.userService = userService;
        this.groupService = groupService;
        this.userGroupRepository = userGroupRepository;
        this.groupMapper = groupMapper;
    }

    @GetMapping("/welcomeInYourGroup")
    public ModelAndView contentView(Authentication authentication) throws NotFound {

        Integer userGroupId = userService.getByLogin(authentication.getName()).getUserGroupId();

        UserGroup userGroup = userGroupRepository.findById(userGroupId)
                .orElseThrow(() -> new NotFound("not found!"));

        GroupDto groupDto = groupMapper.toDtoMyGroup(userGroup);
        List<UserDto> allUsersDtoList = userService.getAllUsersByGroupId(userGroupId);
        ModelAndView modelAndView = new ModelAndView("welcomeInYourGroup");
        modelAndView.addObject("usersList", allUsersDtoList);
        modelAndView.addObject("groupDto", groupDto);

        return modelAndView;
    }

    @GetMapping("/deleteGroup")
    public String userDelete(Authentication authentication) throws NotFound {

        Integer userGroupId = userService.getByLogin(authentication.getName()).getUserGroupId();
        UserGroup userGroup = userGroupRepository.findById(userGroupId).orElseThrow(() -> new NotFound("not found!"));
        groupService.deleteGroup(userGroup.getGroupName());

        return "redirect:/logout";
    }
}