package com.whattodo.viewcontrollers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class PasswordAndRepeatPasswordErrorViewController {

    @GetMapping("/passwordAndRepeatPasswordError")
    public ModelAndView errorUpdateUserView() {

        ModelAndView mav = new ModelAndView("passwordAndRepeatPasswordError");

        return mav;
    }
}
