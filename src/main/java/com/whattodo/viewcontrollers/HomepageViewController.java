package com.whattodo.viewcontrollers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class HomepageViewController {

    @GetMapping("/homepage")
    public ModelAndView contentView() {

        ModelAndView modelAndView = new ModelAndView("homepage");

        return modelAndView;
    }
}