package com.whattodo.viewcontrollers;

import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@Configuration
public class LoginViewController {

    @GetMapping("/login")
    public ModelAndView addViewControllers() {

        return new ModelAndView("login");
    }
}