package com.whattodo.viewcontrollers;

import com.whattodo.dto.TaskDto;
import com.whattodo.dto.UserDto;
import com.whattodo.dto.UserMapper;
import com.whattodo.exception.NotFound;
import com.whattodo.model.User;
import com.whattodo.repository.UserRepository;
import com.whattodo.services.TaskService;
import com.whattodo.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
public class MyProfileViewController {

    private final TaskService taskService;
    private final UserService userService;
    private final UserMapper userMapper;
    private final UserRepository userRepository;


    @Autowired
    public MyProfileViewController(TaskService taskService,
                                   UserService userService,
                                   UserMapper userMapper,
                                   UserRepository userRepository) {

        this.taskService = taskService;
        this.userService = userService;
        this.userMapper = userMapper;
        this.userRepository = userRepository;
    }

    @GetMapping("/myProfile")
    public ModelAndView contentView(Authentication authentication) throws NotFound {

        String loginName = authentication.getName();
        Integer userGroupId = userService.getByLogin(authentication.getName()).getUserGroupId();
        User user = userRepository.findByLogin(loginName).orElseThrow(() -> new NotFound("user doesn't exist"));
        UserDto userDto = userMapper.toDtoUser(user);
        List<TaskDto> allTasksDtoList = taskService.getAllTasks(userGroupId);
        ModelAndView modelAndView = new ModelAndView("myProfileView");
        modelAndView.addObject("tasksList", allTasksDtoList);
        modelAndView.addObject("userDto", userDto);

        return modelAndView;
    }
}