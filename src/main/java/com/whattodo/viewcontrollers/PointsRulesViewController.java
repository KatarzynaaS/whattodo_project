package com.whattodo.viewcontrollers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class PointsRulesViewController {

    @GetMapping("/pointsRules")
    public ModelAndView contentView() {

        ModelAndView modelAndView = new ModelAndView("pointsRules");

        return modelAndView;
    }
}