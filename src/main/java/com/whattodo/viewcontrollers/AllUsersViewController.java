package com.whattodo.viewcontrollers;

import com.whattodo.dto.UserDto;
import com.whattodo.exception.NotFound;
import com.whattodo.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
public class AllUsersViewController {

    private final UserService userService;


    @Autowired
    public AllUsersViewController(UserService userService) {

        this.userService = userService;
    }

    @GetMapping("/users")
    public ModelAndView contentView(Authentication authentication) throws NotFound {

        Integer userGroupId = userService.getByLogin(authentication.getName()).getUserGroupId();
        List<UserDto> allUsersDtoList = userService.getAllUsersByGroupId(userGroupId);
        ModelAndView modelAndView = new ModelAndView("allUsers");
        modelAndView.addObject("usersList", allUsersDtoList);

        return modelAndView;
    }

    @GetMapping("/deleteUser")
    public String userDelete(Authentication authentication) throws NotFound {

        String login = authentication.getName();
        userService.deleteUser(login);

        return "redirect:/logout";
    }
}