package com.whattodo.viewcontrollers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class LoginFailedViewController {

    @PostMapping("/loginFailed")
    public ModelAndView contentView() {

        ModelAndView modelAndView = new ModelAndView("loginFailed");

        return modelAndView;
    }
}