package com.whattodo.viewcontrollers;

import com.whattodo.dto.TaskDto;
import com.whattodo.dto.TaskMapper;
import com.whattodo.exception.NotFound;
import com.whattodo.model.Task;
import com.whattodo.model.User;
import com.whattodo.repository.TaskRepository;
import com.whattodo.repository.UserRepository;
import com.whattodo.services.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class TaskDetailsViewController {

    private final TaskService taskService;
    private final UserRepository userRepository;
    private final TaskRepository taskRepository;
    private final TaskMapper taskMapper;


    @Autowired
    public TaskDetailsViewController(TaskService taskService,
                                     UserRepository userRepository,
                                     TaskRepository taskRepository,
                                     TaskMapper taskMapper) {

        this.taskService = taskService;
        this.userRepository = userRepository;
        this.taskRepository = taskRepository;
        this.taskMapper = taskMapper;
    }

    @GetMapping("/taskDetails")
    public ModelAndView contentView(@RequestParam Integer id, Authentication authentication) throws NotFound {

        String loginName = authentication.getName();
        User loginUser = userRepository.findByLogin(loginName).orElseThrow(() -> new NotFound("user doesn't exist"));
        String loginUserRole = loginUser.getRole().toString();
        Task task = taskRepository.findById(id).orElseThrow(() -> new NotFound("task doesn't exist"));
        TaskDto taskDto = taskMapper.taskToDto(task);

        if (taskDto.getWhoDoLogin() == null) {
            taskDto.setWhoDoLogin(" ---  ");
        }

        ModelAndView modelAndView = new ModelAndView("taskDetails");
        modelAndView.addObject("task", taskDto);
        modelAndView.addObject("loginUserRole", loginUserRole);
        modelAndView.addObject("loginName", loginName);

        return modelAndView;
    }

    @GetMapping("/activate")
    public String activateTest(@RequestParam Integer id) throws NotFound {

        taskService.activateTask(id);

        return "redirect:/myProfile";
    }

    @GetMapping("/start")
    public String startTask(@RequestParam Integer id, Authentication authentication) throws NotFound {

        String loginName = authentication.getName();
        taskService.startTask(id, loginName);

        return "redirect:/myProfile";
    }

    @GetMapping("/finish")
    public String finishTask(@RequestParam Integer id) throws NotFound {

        taskService.finishTask(id);

        return "redirect:/myProfile";
    }

    @GetMapping("/finish_accepted")
    public String finishAcceptedTask(@RequestParam Integer id) throws NotFound {

        taskService.finishedAcceptedTask(id);

        return "redirect:/myProfile";
    }
}