package com.whattodo.viewcontrollers;

import com.whattodo.exception.NotFound;
import com.whattodo.model.Role;
import com.whattodo.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import static com.whattodo.model.Role.ADMIN;

@Controller
public class DeleteGroupWarningViewController {

    private final UserService userService;


    @Autowired
    public DeleteGroupWarningViewController(UserService userService) {

        this.userService = userService;
    }

    @GetMapping("/deleteGroupWarning")
    public ModelAndView contentView(Authentication authentication) throws NotFound {

        Role role = userService.getByLogin(authentication.getName()).getRole();
        ModelAndView modelAndView;

        if (role.equals(ADMIN)) {
            modelAndView = new ModelAndView("deleteGroupWarning");
        }
        else {
            modelAndView = new ModelAndView("deleteOnlyAdmin");
        }

        return modelAndView;
    }
}