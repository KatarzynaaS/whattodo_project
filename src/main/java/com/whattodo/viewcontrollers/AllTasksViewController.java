package com.whattodo.viewcontrollers;

import com.whattodo.dto.TaskDto;
import com.whattodo.exception.NotFound;
import com.whattodo.services.TaskService;
import com.whattodo.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
public class AllTasksViewController {

    private final TaskService taskService;
    private final UserService userService;


    @Autowired
    public AllTasksViewController(TaskService taskService, UserService userService) {

        this.taskService = taskService;
        this.userService = userService;
    }

    @GetMapping("/tasks")
    public ModelAndView contentView(Authentication authentication) throws NotFound {

        Integer userGroupId = userService.getByLogin(authentication.getName()).getUserGroupId();
        List<TaskDto> allTasksDtoList = taskService.getAllTasks(userGroupId);
        ModelAndView modelAndView = new ModelAndView("allTasks");
        modelAndView.addObject("tasksList", allTasksDtoList);

        return modelAndView;
    }
}