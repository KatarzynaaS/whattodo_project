package com.whattodo.dto;

import com.whattodo.model.Task;
import com.whattodo.model.User;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class UserMapper {

    public UserDto toDtoUser(User user) {

        List<Integer> addedTasksIds = user.getAddedTasks().stream()
                .map(Task::getId)
                .collect(Collectors.toList());

        List<Integer> myTasksIds = user.getTasks().stream()
                .map(Task::getId)
                .collect(Collectors.toList());

        return UserDto.builder()
                .id(user.getId())
                .login(user.getLogin())
                .password(user.getPassword())
                .role(user.getRole())
                .points(user.getPoints())
                .addedTasksIds(addedTasksIds)
                .myTasksListIds(myTasksIds)
                .userGroupId(user.getUserGroup().getId())
                .build();
    }
}