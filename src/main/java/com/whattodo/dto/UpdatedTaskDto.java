package com.whattodo.dto;

import com.whattodo.model.Level;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UpdatedTaskDto {

    @NotBlank
    private String name;
    @NotBlank
    private String description;
    private Level level;
}