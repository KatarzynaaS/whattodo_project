package com.whattodo.dto;

import com.whattodo.model.Role;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UserDto {

    private Integer id;
    private String login;
    private String password;
    private Role role;
    private Long points;
    private List<Integer> addedTasksIds;
    private List<Integer> myTasksListIds;
    private Integer userGroupId;
}