package com.whattodo.dto;

import com.whattodo.model.Level;
import com.whattodo.model.TaskStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class TaskDto {

    private Integer id;
    private String name;
    private String description;
    private Level level;
    private String whoAddedLogin;
    private String whoDoLogin;
    private TaskStatus status;
    private Integer taskGroupId;
}