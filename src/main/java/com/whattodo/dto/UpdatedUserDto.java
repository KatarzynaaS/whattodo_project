package com.whattodo.dto;

import com.whattodo.model.Role;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UpdatedUserDto {

    @NotBlank(message = "Password must be filled")
    @Size(min = 5, message = "Password must be at least 5 chars long")
    private String password;
    @NotBlank(message = "Repeat Password must be filled")
    @Size(min = 5, message = "Repeat Password must be at least 5 chars long")
    private String repeatPassword;
    private Role role;
}