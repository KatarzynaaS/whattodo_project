package com.whattodo.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CreateTaskDto {

    @NotBlank
    @Size(min = 3, max = 35, message = "Task name must be at least 3 chars and at most 20 chars long ")
    private String name;
    @NotBlank
    @Size(min = 5, message = "Task name must be at least 5 long ")
    private String description;
    private String level;
    private String whoAddedLogin;
}