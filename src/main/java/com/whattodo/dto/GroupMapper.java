package com.whattodo.dto;

import com.whattodo.model.UserGroup;
import com.whattodo.model.Task;
import com.whattodo.model.User;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class GroupMapper {

    public GroupDto toDtoMyGroup(UserGroup userGroup) {

        List<Integer> usersIds = userGroup.getUsers().stream()
                .map(User::getId)
                .collect(Collectors.toList());

        List<Integer> tasksIds = userGroup.getTasks().stream()
                .map(Task::getId)
                .collect(Collectors.toList());

        return GroupDto.builder()
                .id(userGroup.getId())
                .groupName(userGroup.getGroupName())
                .groupKey(userGroup.getGroupKey())
                .usersIds(usersIds)
                .tasksIds(tasksIds)
                .build();
    }
}