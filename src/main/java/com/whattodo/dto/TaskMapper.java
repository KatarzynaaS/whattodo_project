package com.whattodo.dto;

import com.whattodo.model.Task;
import org.springframework.stereotype.Component;

import static com.whattodo.model.TaskStatus.ACTIVATED;
import static com.whattodo.model.TaskStatus.INACTIVATED;

@Component
public class TaskMapper {

	public TaskDto taskToDto(Task task) {

		String whoDoLogin = "---";

		if (task.getWhoDo() != null) {
			whoDoLogin = task.getWhoDo().getLogin();
		}
		else {
			if (!(task.getStatus() == INACTIVATED || task.getStatus() == ACTIVATED)) {
				whoDoLogin = "account Deleted";
			}
		}

		String whoAddedLogin;

		if (task.getWhoAdded() != null) {
			whoAddedLogin = task.getWhoAdded().getLogin();
		}
		else {
			whoAddedLogin = "account Deleted";
		}

		return TaskDto.builder()
				.id(task.getId())
				.name(task.getName())
				.description(task.getDescription())
				.level(task.getLevel())
				.whoAddedLogin(whoAddedLogin)
				.whoDoLogin(whoDoLogin)
				.status(task.getStatus())
				.taskGroupId(task.getTaskGroup().getId())
				.build();
	}
}