package com.whattodo.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CreateUserDto {

    @NotBlank(message = "Login must be filled")
    @Size(min = 3, message = "Login must be at least 3 chars long")
    private String login;
    @NotBlank(message = "Password must be filled")
    @Size(min = 5, message = "Password must be at least 5 chars long")
    private String password;
    @NotBlank(message = "Password must be filled")
    @Size(min = 5, message = "Password must be at least 5 chars long")
    private String repeatPassword;
    private String role;
    private String groupName;
    private String groupKey;
}