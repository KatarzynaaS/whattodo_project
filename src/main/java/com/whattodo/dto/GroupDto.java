package com.whattodo.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class GroupDto {

    private Integer id;
    private String groupName;
    private String groupKey;
    private List<Integer> usersIds = new ArrayList<>();
    private List<Integer> tasksIds = new ArrayList<>();
}