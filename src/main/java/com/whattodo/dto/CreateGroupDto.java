package com.whattodo.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CreateGroupDto {

    @NotBlank(message = "Group name must be filled")
    @Size(min = 3, max = 16, message = "Group name must be at least 3 chars long and at most 16 chars")
    private String groupName;

    @NotBlank(message = "Key must be filled")
    @Size(min = 2, max = 4, message = "Key must be at least 2 chars long and at most 4 chars")
    private String key;
}