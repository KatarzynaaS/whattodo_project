package com.whattodo.security;

import com.whattodo.model.User;
import com.whattodo.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserDetailsAdapter implements UserDetailsService {

    private final UserRepository userRepository;


    @Autowired
    public UserDetailsAdapter(UserRepository userRepository) {

        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        User user = userRepository.findByLogin(username)
                .orElseThrow(() -> new UsernameNotFoundException(String.format("User %s does not exist!", username)));

        return org.springframework.security.core.userdetails.User.builder()
                .username(username)
                .password("{noop}" + user.getPassword())
                .roles(user.getRole().toString())
                .build();
    }
}