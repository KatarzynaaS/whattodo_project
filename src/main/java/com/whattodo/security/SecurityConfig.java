package com.whattodo.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private final UserDetailsAdapter userDetailsAdapter;


    @Autowired
    public SecurityConfig(UserDetailsAdapter userDetailsAdapter) {

        this.userDetailsAdapter = userDetailsAdapter;
    }

    @Autowired
    public void configureGlobalSecurity(AuthenticationManagerBuilder auth) throws Exception {

        auth.userDetailsService(userDetailsAdapter);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http.csrf().disable()
                .authorizeRequests()
                .antMatchers(HttpMethod.GET, "/homepage").permitAll()
                .antMatchers(HttpMethod.GET, "/createUser").permitAll()
                .antMatchers(HttpMethod.GET, "/createGroup").permitAll()
                .antMatchers(HttpMethod.GET, "/groupIsCreated*").permitAll()
                .antMatchers(HttpMethod.POST, "/createUser").permitAll()
                .antMatchers(HttpMethod.POST, "/createGroup").permitAll()
                .antMatchers(HttpMethod.GET, "/pointsRules").permitAll()
                .antMatchers(HttpMethod.GET, "/styles.css").permitAll()
                .antMatchers(HttpMethod.GET, "/picture.jpg").permitAll()
                .antMatchers(HttpMethod.GET, "/passwordAndRepeatPasswordError").permitAll()
                .antMatchers(HttpMethod.GET, "/loginAlreadyExistError").permitAll()
                .antMatchers(HttpMethod.GET, "/groupDataError").permitAll()
                .antMatchers(HttpMethod.GET, "/groupNameAlreadyExistError").permitAll()
                .anyRequest().authenticated()
                .and()
                .formLogin()
                .loginPage("/login")
                .successForwardUrl("/myProfile")
                .defaultSuccessUrl("/myProfile")
                .permitAll()
                .failureForwardUrl("/loginFailed")
                .and()
                .logout()
                .logoutSuccessUrl("/login")
                .permitAll();
    }
}