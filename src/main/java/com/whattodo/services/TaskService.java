package com.whattodo.services;

import com.whattodo.dto.CreateTaskDto;
import com.whattodo.dto.TaskDto;
import com.whattodo.dto.TaskMapper;
import com.whattodo.dto.UpdatedTaskDto;
import com.whattodo.exception.NotFound;
import com.whattodo.model.*;
import com.whattodo.repository.TaskRepository;
import com.whattodo.repository.UserGroupRepository;
import com.whattodo.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

import static com.whattodo.model.Level.*;
import static com.whattodo.model.Role.*;
import static com.whattodo.model.TaskStatus.*;

@Service
public class TaskService {

	private final TaskRepository taskRepository;
	private final UserRepository userRepository;
	private final UserGroupRepository userGroupRepository;
	private final TaskMapper taskMapper;


	@Autowired
	public TaskService(TaskRepository taskRepository,
	                   UserRepository userRepository,
	                   UserGroupRepository userGroupRepository,
	                   TaskMapper taskMapper) {

		this.taskRepository = taskRepository;
		this.userRepository = userRepository;
		this.userGroupRepository = userGroupRepository;
		this.taskMapper = taskMapper;
	}


	@Transactional
	public List<TaskDto> getAllTasks(Integer myGroupId) {

		return taskRepository.findAll().stream()
				.filter(task -> task.getTaskGroup().getId() == myGroupId)
				.map(i -> taskMapper.taskToDto(i))
				.collect(Collectors.toList());
	}

	@Transactional
	public void updateTask(Integer taskId, UpdatedTaskDto updatedTaskDto) throws NotFound {

		Task task = taskRepository.findById(taskId)
				.orElseThrow(() -> new NotFound(String.format("Task not found for id: %s", taskId)));
		task.setName(updatedTaskDto.getName());
		task.setDescription(updatedTaskDto.getDescription());
		task.setLevel(updatedTaskDto.getLevel());

		taskRepository.save(task);
	}

	@Transactional
	public void activateTask(Integer taskId) throws NotFound {

		Task task = taskRepository.findById(taskId)
				.orElseThrow(() -> new NotFound(String.format("Task not found for id: %s", taskId)));

		task.setStatus(ACTIVATED);

		taskRepository.save(task);
	}

	@Transactional
	public void startTask(Integer taskId, String loginName) throws NotFound {

		Task task = taskRepository.findById(taskId)
				.orElseThrow(() -> new NotFound(String.format("Task not found for id: %s", taskId)));
		User user = userRepository.findByLogin(loginName)
				.orElseThrow(() -> new NotFound(String.format("User not found for loginName: %s", loginName)));

		task.setStatus(STARTED);
		task.setWhoDo(user);

		taskRepository.save(task);

		user.getTasks().add(task);

		userRepository.save(user);
	}

	@Transactional
	public void finishTask(Integer taskId) throws NotFound {

		Task task = taskRepository.findById(taskId)
				.orElseThrow(() -> new NotFound(String.format("Task not found for id: %s", taskId)));

		task.setStatus(FINISHED);

		taskRepository.save(task);
	}

	@Transactional
	public void createNewTask(CreateTaskDto createTaskDto, String loginName) throws NotFound {

		User whoAddedTask = userRepository.findByLogin(loginName)
				.orElseThrow(() -> new NotFound(String.format("Not found for login: %s", loginName)));

		Task task = createTask(createTaskDto, whoAddedTask);

		taskRepository.save(task);

		UserGroup userGroup = userGroupRepository.findById(task.getTaskGroup().getId())
				.orElseThrow(() -> new NotFound(String.format("Not found for group id: %s", task.getTaskGroup().getId())));

		userGroup.getTasks().add(task);
		whoAddedTask.getAddedTasks().add(task);
	}

	@Transactional
	public void finishedAcceptedTask(Integer taskId) throws NotFound {

		Task task = taskRepository.findById(taskId)
				.orElseThrow(() -> new NotFound(String.format("Task not found for id: %s", taskId)));
		task.setStatus(FINISHED_ACCEPTED);

		if (task.getWhoDo() != null) {
			setPointsForFinishedTask(task);
		}

		taskRepository.save(task);
	}

	private Task createTask(CreateTaskDto createTaskDto, User whoAddedTask) {

		return Task.builder()
				.name(createTaskDto.getName())
				.description(createTaskDto.getDescription())
				.level(Level.valueOf(createTaskDto.getLevel()))
				.whoAdded(whoAddedTask)
				.whoDo(null)
				.status(INACTIVATED)
				.taskGroup(whoAddedTask.getUserGroup())
				.build();
	}

	private void setPointsForFinishedTask(Task task) {

		User user = task.getWhoDo();
		Integer taskPoints = howManyPointsForTask(task.getLevel(), user.getRole());
		Long sumPoints = user.getPoints() + taskPoints;
		user.setPoints(sumPoints);
	}

	private Integer howManyPointsForTask(Level taskLevel, Role userRole) {

		if (taskLevel.equals(EASE)) {
			if (userRole.equals(JUNIOR)) return 4;
			if (userRole.equals(MIDDLE)) return 2;
			if (userRole.equals(SENIOR) || userRole.equals(ADMIN)) return 1;
		}

		if (taskLevel.equals(MEDIUM)) {
			if (userRole.equals(MIDDLE)) return 4;
			if (userRole.equals(SENIOR) || userRole.equals(ADMIN)) return 2;
		}

		if (taskLevel.equals(HARD) && ((userRole.equals(SENIOR) || userRole.equals(ADMIN)))) return 4;

		return 0;
	}
}