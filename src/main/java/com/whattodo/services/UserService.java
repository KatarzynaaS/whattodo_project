package com.whattodo.services;

import com.whattodo.dto.CreateUserDto;
import com.whattodo.dto.UpdatedUserDto;
import com.whattodo.dto.UserDto;
import com.whattodo.dto.UserMapper;
import com.whattodo.exception.AlreadyExists;
import com.whattodo.exception.InvalidData;
import com.whattodo.exception.NotFound;
import com.whattodo.model.Role;
import com.whattodo.model.Task;
import com.whattodo.model.User;
import com.whattodo.model.UserGroup;
import com.whattodo.repository.UserGroupRepository;
import com.whattodo.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserService {

	private final UserRepository userRepository;
	private final UserGroupRepository userGroupRepository;
	private final UserMapper userMapper;


	@Autowired
	public UserService(UserRepository userRepository,
	                   UserGroupRepository userGroupRepository,
	                   UserMapper userMapper) {

		this.userRepository = userRepository;
		this.userGroupRepository = userGroupRepository;
		this.userMapper = userMapper;
	}

	@Transactional
	public List<UserDto> getAllUsersByGroupId(Integer groupId) {

		return userRepository.findAll().stream()
				.filter(user -> user.getUserGroup().getId() == groupId)
				.map(i -> userMapper.toDtoUser(i))
				.collect(Collectors.toList());
	}

	@Transactional
	public UserDto getByLogin(String login) throws NotFound {

		return userRepository.findByLogin(login)
				.map(i -> userMapper.toDtoUser(i))
				.orElseThrow(() -> new NotFound("Not found"));
	}

	@Transactional
	public void updateUser(UpdatedUserDto updatedUserDto, String login) throws NotFound, InvalidData {

		User user = userRepository.findByLogin(login)
				.orElseThrow(() -> new NotFound(String.format("User %s doesn't exist", login)));

		if (updatedUserDto.getPassword().equals(updatedUserDto.getRepeatPassword())) {
			user.setRole(updatedUserDto.getRole());
			user.setPassword(updatedUserDto.getPassword());
		}
		else {
			throw new InvalidData("Password and Repeated Password must be the same!");
		}
	}

	@Transactional
	public void deleteUser(String login) throws NotFound {

		User user = userRepository.findByLogin(login)
				.orElseThrow(() -> new NotFound(String.format("User %s doesn't exist", login)));

		List<Task> addedTasksList = user.getAddedTasks();

		for (Task task : addedTasksList) {
			task.setWhoAdded(null);
		}

		List<Task> taskList = user.getTasks();
		for (Task task : taskList) {
			task.setWhoDo(null);
		}

		UserGroup userGroup = user.getUserGroup();
		userGroup.getUsers().remove(user);
		userRepository.delete(user);
	}

	@Transactional
	public void createNewUser(CreateUserDto createUserDto) throws InvalidData, AlreadyExists, NotFound {

		if (userRepository.existsByLogin(createUserDto.getLogin())) {
			throw new AlreadyExists("User with same login already exists!");
		}

		if (!(createUserDto.getPassword().equals(createUserDto.getRepeatPassword()))) {
			throw new InvalidData("Password and Repeated Password must be the same");
		}

		UserGroup userGroup = userGroupRepository.findByGroupName(createUserDto.getGroupName())
				.orElseThrow(() -> new NotFound(String.format("Group %s not found!", createUserDto.getGroupName())));

		if (!userGroup.getGroupKey().equals(createUserDto.getGroupKey())) {
			throw new NotFound("Invalid group data");
		}

		User user = createUser(createUserDto, userGroup);
		userRepository.save(user);
		userGroup.getUsers().add(user);
	}

	private User createUser(CreateUserDto createUserDto, UserGroup userGroup) {

		return User.builder()
				.login(createUserDto.getLogin())
				.role(Role.valueOf(createUserDto.getRole()))
				.password(createUserDto.getPassword())
				.points(0L)
				.addedTasks(new ArrayList<>())
				.tasks(new ArrayList<>())
				.userGroup(userGroup)
				.build();
	}
}