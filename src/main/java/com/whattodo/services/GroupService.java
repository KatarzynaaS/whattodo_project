package com.whattodo.services;

import com.whattodo.dto.CreateGroupDto;
import com.whattodo.exception.AlreadyExists;
import com.whattodo.exception.NotFound;
import com.whattodo.model.UserGroup;
import com.whattodo.repository.TaskRepository;
import com.whattodo.repository.UserGroupRepository;
import com.whattodo.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class GroupService {

	private final UserGroupRepository userGroupRepository;
	private final TaskRepository taskRepository;
	private final UserRepository userRepository;


	@Autowired
	public GroupService(UserGroupRepository userGroupRepository,
	                    TaskRepository taskRepository,
	                    UserRepository userRepository) {

		this.userGroupRepository = userGroupRepository; this.taskRepository = taskRepository;
		this.userRepository = userRepository;
	}

	@Transactional
	public void createNewGroup(CreateGroupDto createGroupDto) throws AlreadyExists {

		if (userGroupRepository.existsUserGroupByGroupName(createGroupDto.getGroupName())) {
			throw new AlreadyExists(String.format("Group %s already exist", createGroupDto.getGroupName()));
		}

		UserGroup newUserGroup = createGroup(createGroupDto);

		userGroupRepository.save(newUserGroup);
	}

	@Transactional
	public void deleteGroup(String name) throws NotFound {

		UserGroup group = userGroupRepository.findByGroupName(name).orElseThrow(() -> new NotFound(String.format(
				"Group %s doesn't exist", name)));

		taskRepository.deleteAll(group.getTasks()); userRepository.deleteAll(group.getUsers());
		userGroupRepository.delete(group);
	}

	private UserGroup createGroup(CreateGroupDto createGroupDto) {

		return UserGroup.builder().groupName(createGroupDto.getGroupName()).groupKey(createGroupDto.getKey()).build();
	}
}