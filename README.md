WhatToDo Project - author: Katarzyna Sobota

Introduction
WhatToDo it's Spring Application to organize tasks for a group of users.
Each user registers an account with their experience level, 
and can add and take tasks.
After adding , the task  is waiting for Admin acceptance and 
then users can take them according to their experience. 
After finishing their tasks and Admin approval users are rewarded with points.


Prerequisites
Java 1.8

Used Tools:

Frameworks
Hibernate - for map from Java classes to database tables, 
and from Java data types to SQL data types, provide data query and 
retrieval facilities. In the local environment application uses a profile 
using MySql database and in mobile environment (on serwer) uses h2 database.   

Spring 
Spring Model–view–controller - to manage incoming requests and
redirect to proper response. Controller will map the http request 
to corresponding methods. It acts as a gate that directs 
the incoming information. It switches between going into model or view.
Spring security - to provide authentication and authorization to application

Libraries
Mocito -  for effective unit testing of  application

Lombok - to reduce code  through generate getters and setters 
for objects automatically

Extended possibilities
According to the Open/Closed rule,the application can be extended 
by new solutions e.g. creating groups of users, developing users profiles, 
adding dates of operations, improving front view etc.
